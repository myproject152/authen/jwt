package com.nms.uoc.controller;

import com.nms.uoc.model.dto.UserTableDTO;
import com.nms.uoc.model.entity.GroupTable;
import com.nms.uoc.model.entity.PermissionTable;
import com.nms.uoc.model.entity.RoleTable;
import com.nms.uoc.model.entity.UserTable;
import com.nms.uoc.service.IUserTableService;
import com.nms.uoc.service.impl.UserTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.Set;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/user")
public class UserTableController {
    @Autowired
    IUserTableService service;


    @GetMapping("/{id}")
    @PreAuthorize("hasAnyAuthority(@securityService.getPermissionView('user'))")
    public ResponseEntity getById(@PathVariable int id) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(service.findById(id));
    }

    @GetMapping()
    @PreAuthorize("hasAnyAuthority(@securityService.getPermissionView('user'))")
    public ResponseEntity getById1() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(service.getAll());
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyAuthority(@securityService.getPermissionDelete('user'))")
    public ResponseEntity update(@PathVariable int id, @RequestBody UserTableDTO userDTO) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(service.update(id, userDTO));
    }

    @GetMapping("/permission/{id}")
    public ResponseEntity getPermission(@PathVariable int id) {
        Set<String> permissions = new HashSet<>();
        UserTable userTable = service.findById(id);

//        userTable.getPermissions().stream().map(data -> permission.add(data.getName()));

        for (PermissionTable permission: userTable.getPermissions()) {
            permissions.add(permission.getName());
        }

        for (RoleTable role: userTable.getRoles()) {
            for (PermissionTable permission: role.getPermissions()) {
                permissions.add(permission.getName());
            }
        }

        for (GroupTable group: userTable.getGroups()) {
            for (PermissionTable permission: group.getPermissions()) {
                permissions.add(permission.getName());
            }
        }
        return ResponseEntity.status(HttpStatus.OK)
                .body(permissions);
    }
}
