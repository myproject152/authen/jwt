package com.nms.uoc.repository;

import com.nms.uoc.model.entity.UserEntity;
import com.nms.uoc.model.entity.UserTable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserTableRepository extends JpaRepository<UserTable, Integer> {
    Optional<UserTable> findByUsername(String username);

}
