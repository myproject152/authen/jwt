package com.nms.uoc.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserTableDTO {
    private String username;
    private String password;
    private String firstName;
    private String phoneNumber;
    private String email;
    private Integer status;

}
