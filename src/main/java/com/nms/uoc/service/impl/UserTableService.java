package com.nms.uoc.service.impl;

import com.nms.uoc.config.exceptions.AppException;
import com.nms.uoc.config.exceptions.ErrorResponseBase;
import com.nms.uoc.config.security.model.SysUserDetails;
import com.nms.uoc.model.dto.UserTableDTO;
import com.nms.uoc.model.entity.GroupTable;
import com.nms.uoc.model.entity.PermissionTable;
import com.nms.uoc.model.entity.RoleTable;
import com.nms.uoc.model.entity.UserTable;
import com.nms.uoc.repository.UserTableRepository;
import com.nms.uoc.service.IUserTableService;
import com.nms.uoc.utils.PasswordUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class UserTableService implements IUserTableService {

    @Autowired
    private UserTableRepository userRepository;

    @Override
    public UserTable creat(UserTableDTO userDTO) {
        return null;
    }

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserTable update(int id, UserTableDTO userDTO) {
        UserTable userTable = findById(id);
        BeanUtils.copyProperties(userDTO, userTable);
        userTable.setPassword(passwordEncoder.encode(userTable.getPassword()));
        try {
            return userRepository.save(userTable);
        } catch (Exception e){
            throw new AppException(e);
        }
    }

    public UserTable findById(int id) {
        if (userRepository.findById(id).isEmpty()){
            throw new AppException(ErrorResponseBase.NOT_FOUND);
        }
        return userRepository.findById(id).get();
    }

    public List<UserTable> getAll() {
        return userRepository.findAll();
    }

    @Override
    public UserDetails loadUserByUsername(String username){
        Optional<UserTable> user = userRepository.findByUsername(username);
        SysUserDetails sysUserDetails = new SysUserDetails();
        Set<GrantedAuthority> permissions = new HashSet<>();

        if (user.isPresent()) {
            BeanUtils.copyProperties(user.get(), sysUserDetails);

            for (PermissionTable permission : sysUserDetails.getPermissions()) {
                permissions.add(new SimpleGrantedAuthority(permission.getName()));
            }

            for (RoleTable role : sysUserDetails.getRoles()) {
                for (PermissionTable permission : role.getPermissions()) {
                    permissions.add(new SimpleGrantedAuthority(permission.getName()));
                }
            }

            for (GroupTable group : sysUserDetails.getGroups()) {
                for (PermissionTable permission : group.getPermissions()) {
                    permissions.add(new SimpleGrantedAuthority(permission.getName()));
                }
            }
            sysUserDetails.setAuthorities(permissions);
            return sysUserDetails;
        } else {
            return null;
//            throw new AppException(ErrorResponseBase.USER_NOT_FOUND);
        }
    }
}
