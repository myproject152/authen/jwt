package com.nms.uoc.service;

import com.nms.uoc.model.dto.UserTableDTO;
import com.nms.uoc.model.entity.UserTable;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.List;

public interface IUserTableService extends UserDetailsService {
    UserTable creat(UserTableDTO userDTO);
    UserTable update(int id, UserTableDTO userDTO);
    UserTable findById(int id);
    List<UserTable> getAll();
}
