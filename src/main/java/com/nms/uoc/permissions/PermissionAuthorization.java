package com.nms.uoc.permissions;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("securityService")
public class PermissionAuthorization {

    List<String> listPermission = new ArrayList<>();

    public List<String> getPermissionView(String entity) {
        listPermission.clear();
        listPermission.add("ROLE_Admin");
        listPermission.add("PERMISSION_Permission.Partner." + entity + "." + PermissionList.PermissionDefault.VIEW.name());
        listPermission.add("view");
        listPermission.add("delete");
        listPermission.add("update");
        listPermission.add("add");
        return listPermission;
    }

    public List<String> getPermissionDelete(String entity) {
        listPermission.clear();
        listPermission.add("PERMISSION_Permission.Partner." + entity + "." + PermissionList.PermissionDefault.DELETE.name());
        listPermission.add("delete");
        return listPermission;
    }

    public List<String> getPermissionUpdate(String entity) {
        listPermission.clear();
        listPermission.add("PERMISSION_Permission.Partner." + entity + "." + PermissionList.PermissionDefault.UPDATE.name());
        listPermission.add("update");
        return listPermission;
    }

    public List<String> getPermissionAdd(String entity) {
        listPermission.clear();
        listPermission.add("PERMISSION_Permission.Partner." + entity + "." + PermissionList.PermissionDefault.ADD.name());
        listPermission.add("add");
        return listPermission;
    }

    public List<String> getPermission() {
        listPermission.clear();
        listPermission.add("ROLE_Admin");
        listPermission.add("PERMISSION_Permission." + PermissionList.PartnerPermission.PARTNER_VIEW_LIST_PERMISSION);
        return listPermission;
    }

}
